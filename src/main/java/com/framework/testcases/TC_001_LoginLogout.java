package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC_001_LoginLogout extends ProjectMethods {
	
	@BeforeTest
	public void setData()
	{
		testCaseName="TC_001_LoginLogout";
		testDescription="Login into Leaftaps";
		testNodes="Leads";
		author="Pooja";
		category="smoke";
		dataSheetName="TC001";
	
		
	}
	@Test(dataProvider="fetchData")
	public void login(String username, String password)
	{
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickLogout();
		
		
		
		
	}

}

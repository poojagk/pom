package com.framework.pages;

import org.openqa.selenium.WebElement;

import com.framework.design.ProjectMethods;

public class CreateLead extends ProjectMethods {
	
	public CreateLead()
	{
		
	}

	public CreateLead enterCompanyname() {
		WebElement eleCompanyname = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompanyname, "TestLeaf");
		return this; 
	}
	public CreateLead enterFirstname() {
		WebElement eleFirstname = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFirstname, "Pooja");
		return this;
	}
		
		public CreateLead enterLastname() {
			WebElement eleLastname = locateElement("id", "createLeadForm_lastName");
			clearAndType(eleLastname, "GK");
			return this;
	}
	public ViewLeadPage clicksubmit() {
		WebElement elesubmit = locateElement("name", "submitButton");
	    click(elesubmit);
	    /*HomePage hp = new HomePage();
	    return hp;*/ 
	    return new ViewLeadPage();
}
}
